const http = require('http');

const server = http.createServer((req, res) => {
  res.setHeader('Content-Type', 'text/html; charset=utf-8');

  if (req.url === '/') {
    // Tela inicial com texto de apresentação
    res.write(`
      <html>
        <head>
          <title>Explorando a Vida em Outros Planetas</title>
          <style>
            body {
              font-family: Arial, sans-serif;
              margin: 20px;
            }
  
            h1 {
              color: #333;
            }
  
            p {
              color: #666;
            }
  
            ul {
              list-style-type: none;
              padding: 0;
            }
  
            li {
              margin-bottom: 10px;
            }
  
            a.button {
              display: inline-block;
              padding: 10px 20px;
              background-color: #007bff;
              color: #fff;
              text-decoration: none;
              border-radius: 4px;
            }
  
            a.button:hover {
              background-color: #0056b3;
            }
          </style>
        </head>
        <body>
          <h1>Bem-vindo à Exploração da Vida em Outros Planetas</h1>
          <p>Este é um servidor de demonstração com informações sobre a possibilidade de vida em outros planetas.</p>
          <p>Escolha uma opção abaixo para explorar mais:</p>
          <ul>
            <li><a href="/planeta1" class="button">Planeta 1</a></li>
            <li><a href="/planeta2" class="button">Planeta 2</a></li>
          </ul>
        </body>
      </html>
    `);
  } else if (req.url === '/planeta1') {
    // Tela do Planeta 1
    res.write(`
      <html>
        <head>
          <title>Planeta 1 - Vida em Outros Planetas</title>
          <style>
            body {
              font-family: Arial, sans-serif;
              margin: 20px;
            }
  
            h1 {
              color: #333;
            }
  
            p {
              color: #666;
            }
  
            button {
              padding: 10px 20px;
              background-color: #007bff;
              color: #fff;
              border: none;
              cursor: pointer;
            }
  
            a.button {
              display: inline-block;
              padding: 10px 20px;
              background-color: #007bff;
              color: #fff;
              text-decoration: none;
              border-radius: 4px;
            }
  
            a.button:hover {
              background-color: #0056b3;
            }
          </style>
        </head>
        <body>
          <h1>Planeta 1</h1>
          <p>Este é o Planeta 1. Acredita-se que as condições aqui permitam o desenvolvimento de formas de vida microscópicas.</p>
          <button onclick="alert('Vida microscópica foi descoberta no Planeta 1!')">Descobrir Vida</button>
          <br>
          <a href="/" class="button">Voltar à página inicial</a>
        </body>
      </html>
    `);
  } else if (req.url === '/planeta2') {
    // Tela do Planeta 2
    res.write(`
      <html>
        <head>
          <title>Planeta 2 - Vida em Outros Planetas</title>
          <style>
            body {
              font-family: Arial, sans-serif;
              margin: 20px;
            }
  
            h1 {
              color: #333;
            }
  
            p {
              color: #666;
            }
  
            button {
              padding: 10px 20px;
              background-color: #007bff;
              color: #fff;
              border: none;
              cursor: pointer;
            }
  
            a.button {
              display: inline-block;
              padding: 10px 20px;
              background-color: #007bff;
              color: #fff;
              text-decoration: none;
              border-radius: 4px;
            }
  
            a.button:hover {
              background-color: #0056b3;
            }
          </style>
        </head>
        <body>
          <h1>Planeta 2</h1>
          <p>O Planeta 2 é um mundo gasoso e hostil, tornando extremamente improvável a existência de qualquer forma de vida conhecida.</p>
          <button onclick="alert('Nenhuma forma de vida foi encontrada no Planeta 2.')">Explorar Planeta</button>
          <br>
          <a href="/" class="button">Voltar à página inicial</a>
        </body>
      </html>
    `);
  } else {
    // Página de erro para URLs desconhecidas
    res.statusCode = 404;
    res.write(`
      <html>
        <head>
          <title>Página não encontrada</title>
          <style>
            body {
              font-family: Arial, sans-serif;
              margin: 20px;
            }
  
            h1 {
              color: #333;
            }
  
            p {
              color: #666;
            }
  
            a.button {
              display: inline-block;
              padding: 10px 20px;
              background-color: #007bff;
              color: #fff;
              text-decoration: none;
              border-radius: 4px;
            }
  
            a.button:hover {
              background-color: #0056b3;
            }
          </style>
        </head>
        <body>
          <h1>Erro 404: Página não encontrada</h1>
          <p>A página que você está procurando não foi encontrada.</p>
          <a href="/" class="button">Voltar à página inicial</a>
        </body>
      </html>
    `);
  }

  res.end();
});

const port = 3000;
server.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`);
});
