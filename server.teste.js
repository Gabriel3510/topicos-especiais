const request = require('supertest');
const app = require('./server');

describe('Testando rota GET /', () => {
  test('Deve retornar o status 200 e a mensagem "Hello, world!"', async () => {
    const response = await request(app).get('/');
    expect(response.status).toBe(200);
    expect(response.text).toBe('Hello, world!');
  });
});
